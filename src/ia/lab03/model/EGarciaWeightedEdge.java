package ia.lab03.model;

import org.jgrapht.graph.DefaultWeightedEdge;

public class EGarciaWeightedEdge extends DefaultWeightedEdge {
	private static final long serialVersionUID = 2364156479897456877L;
	@Override
	public String toString() {		
		return this.getSource() + "-" + this.getTarget() + ":" + (int) getWeight();
	}
}
