package ia.lab03.model;

import ia.lab03.GraphLab;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.Toolkit;
import java.awt.geom.Rectangle2D;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JFrame;
import org.jgraph.JGraph;
import org.jgraph.graph.DefaultGraphCell;
import org.jgraph.graph.GraphConstants;
import org.jgrapht.ext.JGraphModelAdapter;
import org.jgrapht.graph.DirectedWeightedPseudograph;


public class Graph {
	private int branchingFactor;
	private final String vertexTag = "V";
	private JGraphModelAdapter<String, EGarciaWeightedEdge> jmaGraph;
	
	private DirectedWeightedPseudograph<String, EGarciaWeightedEdge>  dwpGraph;
	public DirectedWeightedPseudograph<String, EGarciaWeightedEdge> get() {
		return dwpGraph;
	}

	public Graph(String path) {		
		readGraph(openFile(path));
	}
	
	private void readGraph(ArrayList<String> fileContents){
		boolean first = true;
		int currentParentNode = 0;
		final String header = "Exception during the reading of the graph array: ";
		if (fileContents == null){
			System.out.println(header + "Empty file contents");
			System.exit(4);
		}
			
		for (String line : fileContents){
			//Ignore comments and empty lines
			if (line.length() == 0) 
				continue;
			if (line.charAt(0) == '#')
				continue;
				
			String[] successorEdgeWeightArray = line.trim().split("\\s+");

			//Determine the branching factor based on the maximum possible successors any given node can have
			if (first){
				//Structure Graph using jgrapht
				dwpGraph = new DirectedWeightedPseudograph<String, EGarciaWeightedEdge>(EGarciaWeightedEdge.class); 
				branchingFactor = successorEdgeWeightArray.length;
				if(GraphLab.verbose)
					System.out.println("Branching Factor: " + this.branchingFactor);
				//Add the vertexes to the graph
				for (int i = 1; i<= branchingFactor; i++){
					this.dwpGraph.addVertex(vertexTag+i);
				}				
				first = false;				
			}
			currentParentNode++;
			if(GraphLab.verbose)
				System.out.println("Parent: " + currentParentNode );
			int currentSuccessorNode = 0;
			
			for(String successorEdgeWeight: successorEdgeWeightArray){
				if(!successorEdgeWeight.matches("\\d+")){
					System.out.println(header + "Invalid input: " +  successorEdgeWeight);
					System.exit(5);
				}
				currentSuccessorNode ++;
				//Add successor to current vertex and its edge weight
				int successorEdgeWeightInt = Integer.parseInt(successorEdgeWeight);
				if(successorEdgeWeightInt < 1)
					continue;
				if(GraphLab.verbose){
					System.out.print(
							" P? " + dwpGraph.containsVertex(vertexTag+currentParentNode) + " S? " + dwpGraph.containsVertex(vertexTag+currentSuccessorNode) 
							+ ", successor: " + currentSuccessorNode 
							+ " edgeWeight: " + successorEdgeWeight + "\t");
					System.out.println();
					for(int i = 1; i<= this.branchingFactor; i++)
						System.out.println(vertexTag+ i + "?:" + this.dwpGraph.containsVertex(vertexTag + i));

				}
				EGarciaWeightedEdge currentEdge = dwpGraph.addEdge(vertexTag+currentParentNode, vertexTag+ currentSuccessorNode);
				 dwpGraph.setEdgeWeight(currentEdge, successorEdgeWeightInt); 
			}
			System.out.println();
			
		}
	}
	
	private ArrayList<String> openFile(String path){ 
		try{
			File inputFile = new File(path);
			if (inputFile.length() == 0){
				System.out.println("Empty input file, :'( exiting...");
				System.exit(3);
			}
			BufferedReader bufferedReader = new BufferedReader(new FileReader(inputFile));
			ArrayList<String> fileContents = new ArrayList<String>();
			String line;
			while( (line = bufferedReader.readLine()) != null){
				fileContents.add(line);
			}
			bufferedReader.close();
			return fileContents;
		} catch(IOException ioe) {
			System.out.println("Exception during input file reading: " + ioe.getMessage());
			System.exit(2);
			//Necessary to satisfy compiler needs. ;)
			return null;
		}
	}
	
	
	public void graphView(){
		String header = "Exception during the display of the graph: ";
		this.jmaGraph = new JGraphModelAdapter<String, EGarciaWeightedEdge>( dwpGraph );
	    JGraph jgraph = new JGraph(this.jmaGraph);	    
	    this.adjustDisplaySettings(jgraph);
	   
	    JFrame frame = new JFrame();
	    //frame.setSize(600, 600);
	    frame.getContentPane().add(jgraph);
	    frame.setTitle("GraphLab: Erick Garcia");
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    //frame.pack();
	    frame.setExtendedState(JFrame.MAXIMIZED_BOTH); 
	    this.positionVertexes();
	    frame.setVisible(true);
	    while (true) {
	    	try {
				Thread.sleep(2000);
			} catch (InterruptedException e) {
				System.out.println(header + e.getMessage());
				System.exit(6);
			}
	    }
	}
	
	private void positionVertexes(){
		int spiralDelta = 10;
		//Screen size data.
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int resolution = Toolkit.getDefaultToolkit().getScreenResolution();
		double width = screenSize.getWidth(), height = screenSize.getHeight();
		
		if(GraphLab.verbose)
			System.out.println("Resolution: " + resolution + " witdh: " + width + " height: " + height);
		int howManyVertexes = dwpGraph.vertexSet().size();
		double x = 0, y = 0, xCenter = width/2, yCenter = height/2, angle = 0, deltaAngle = (2* Math.PI)/howManyVertexes, r = 80;
		for (String vertex : dwpGraph.vertexSet()){
			if (r < height -20 && r < width -20)
				r += spiralDelta;

			x = xCenter + r * Math.cos(angle);
			y = yCenter + r * Math.sin(angle);
			
			positionVertexAt(vertex,(int) x,(int) y);
			angle += deltaAngle;		
		}
	}
	
	private void adjustDisplaySettings(JGraph jgraph) {
        //jg.setPreferredSize(DEFAULT_SIZE);
        jgraph.setBackground(Color.GRAY);
    }

    private void positionVertexAt(Object vertex, int x, int y) {
        DefaultGraphCell cell = this.jmaGraph.getVertexCell(vertex);
        Map<?, ?> attr = cell.getAttributes();
        Rectangle2D b = GraphConstants.getBounds(attr);

        GraphConstants.setBounds(attr, new Rectangle(x, y, (int) b.getWidth(), (int) b.getHeight()));

        Map<DefaultGraphCell, Map<?, ?>> cellAttr = new HashMap<DefaultGraphCell, Map<?, ?>>();
        cellAttr.put(cell, attr);
        this.jmaGraph.edit(cellAttr, null, null, null);
    }

}
