package ia.lab03;

import ia.lab03.model.Graph;

public class GraphLab {
	
	public static boolean verbose;

	public static void main(String[] args) {
		if (args.length == 0 || args.length > 2) {
			printUsage();
			System.exit(1);
		}
		verbose = (args.length == 2) ? true : false;
				
		Graph graph = new Graph(args[0]);
		graph.graphView();
	}
	
	private static void printUsage(){
		System.out.println("usage: GraphLab <input filename full path> <verbose>");
	}
	

}
