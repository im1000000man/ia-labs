package ia.lab17;

import java.util.ArrayList;
import java.util.Random;

//Source and example based on: http://www.vankouteren.eu/blog/2007/10/k-means-clustering-implementation-in-java/

public class KMeans
{
    private Boolean DEBUG = false;

    private int NUM_CLUSTERS = 1;    // Total clusters.
    private int TOTAL_DATA;      // Total data points.
        
    private ArrayList<Data> sourceDataset;
    public static ArrayList<Data> dataSet = new ArrayList<Data>();
    public static ArrayList<Centroid> centroids = new ArrayList<Centroid>();
        
    public KMeans(ArrayList<Data> sourceDataset, int nClusters, boolean debug)
    {
        this.sourceDataset = sourceDataset;
        this.TOTAL_DATA = this.sourceDataset.size();
        //Set the initial centroids by random
        Random random = new Random();
        while (centroids.size() < NUM_CLUSTERS) {
        	int randomIndex = random.nextInt(TOTAL_DATA);
        	this.NUM_CLUSTERS = nClusters;
        	this.DEBUG = debug;
        	Data auxData = this.sourceDataset.get(randomIndex);
        	Centroid auxCentroid = new Centroid(auxData.X(), auxData.Y() );
        	if(!centroids.contains(auxCentroid))
                centroids.add(auxCentroid);
        }
        if (DEBUG) {
        	System.out.println("Centroids initialized at:");
        	this.printCentroids();
        }
        this.kMeanCluster();
    }
    
    public void printCentroids(){
    	  // Print out centroid results.        
        for(int i = 0; i < this.NUM_CLUSTERS; i++)
        {
            System.out.println("     (" + centroids.get(i).X() + ", " + centroids.get(i).Y() + ")");
        }
        System.out.print("\n");	
    }
    
    public void printClusteringResults(){
    	  // Print out clustering results.
        for(int i = 0; i < this.NUM_CLUSTERS; i++)
        {
            System.out.println("Cluster " + (i+1) + " includes:");
            for(int j = 0; j < dataSet.size(); j++)
            {
                if(dataSet.get(j).cluster() == i){
                    System.out.println("     " + (j+1) +"(" + dataSet.get(j).X() + ", " + dataSet.get(j).Y() + ")");
                }
            } // j
            System.out.println();
        } // i
    }
    
    private void kMeanCluster()
    {
        final double bigNumber = Math.pow(10, 10);    // some big number that's sure to be larger than our data range.
        double minimum = bigNumber;                   // The minimum value to beat. 
        double distance = 0.0;                        // The current minimum value.
        int sampleNumber = 0;
        int cluster = 0;
        boolean isStillMoving = true;
        Data newData = null;
        
        // Add in new data, one at a time, recalculating centroids with each new one. 
        while(dataSet.size() < TOTAL_DATA)
        {

        	newData = this.sourceDataset.get(sampleNumber);
        	dataSet.add(newData);
            minimum = bigNumber;
            for(int i = 0; i < NUM_CLUSTERS; i++)
            {
                distance = dist(newData, centroids.get(i));
                if(distance < minimum){
                    minimum = distance;
                    cluster = i;
                }
            }
            newData.cluster(cluster);
            
            // calculate new centroids.
            for(int i = 0; i < NUM_CLUSTERS; i++)
            {
                int totalX = 0;
                int totalY = 0;
                int totalInCluster = 0;
                for(int j = 0; j < dataSet.size(); j++)
                {
                    if(dataSet.get(j).cluster() == i){
                        totalX += dataSet.get(j).X();
                        totalY += dataSet.get(j).Y();
                        totalInCluster++;
                    }
                }
                if(totalInCluster > 0){
                    centroids.get(i).X(totalX / totalInCluster);
                    centroids.get(i).Y(totalY / totalInCluster);
                }
            }
            sampleNumber++;
            if(DEBUG){
            	System.out.println("Current Centroids: ");
            	this.printCentroids();
            	System.out.println("Current Clusters State: ");
            	this.printClusteringResults();
            	System.out.println("-- -- \n \n");
            }
        }
        
        // Now, keep shifting centroids until equilibrium occurs.
        while(isStillMoving)
        {
            // calculate new centroids.
            for(int i = 0; i < NUM_CLUSTERS; i++)
            {
                int totalX = 0;
                int totalY = 0;
                int totalInCluster = 0;
                for(int j = 0; j < dataSet.size(); j++)
                {
                    if(dataSet.get(j).cluster() == i){
                        totalX += dataSet.get(j).X();
                        totalY += dataSet.get(j).Y();
                        totalInCluster++;
                    }
                }
                if(totalInCluster > 0){
                    centroids.get(i).X(totalX / totalInCluster);
                    centroids.get(i).Y(totalY / totalInCluster);
                }
            }
            
            // Assign all data to the new centroids
            isStillMoving = false;
            
            for(int i = 0; i < dataSet.size(); i++)
            {
                Data tempData = dataSet.get(i);
                minimum = bigNumber;
                for(int j = 0; j < NUM_CLUSTERS; j++)
                {
                    distance = dist(tempData, centroids.get(j));
                    if(distance < minimum){
                        minimum = distance;
                        cluster = j;
                    }
                }
                tempData.cluster(cluster);
                if(tempData.cluster() != cluster){
                    tempData.cluster(cluster);
                    isStillMoving = true;
                }
            }
        }
        return;
    }
    
    /**
     * // Calculate Euclidean distance.
     * @param d - Data object.
     * @param c - Centroid object.
     * @return - double value.
     */
    private double dist(Data d, Centroid c)
    {
        return Math.sqrt(Math.pow((c.Y() - d.Y()), 2) + Math.pow((c.X() - d.X()), 2));
    }
    
}
