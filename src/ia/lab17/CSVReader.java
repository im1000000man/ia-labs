package ia.lab17;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class CSVReader {
	private BufferedReader bReader;
	private ArrayList<Data> dataList;
	private String[] headers;

	// Headers used to specify which two columns to use during the kmeans
	// clustering
	public CSVReader(String filePath, String header1, String header2) {
		try {
			bReader = new BufferedReader(new FileReader(new File(filePath)));
		} catch (FileNotFoundException e) {
			System.out.println("Error: " + e.getMessage());
			System.exit(1);
		}
		fillData(header1, header2);
	}

	private void fillData(String header1, String header2) {
		String line;
		String[] lineData;
		int x = -1, y = -1;

		try {
			// Get column headers
			line = this.bReader.readLine();
			if (line != null) {
				lineData = line.split(",");
				headers = lineData;
				this.dataList = new ArrayList<Data>();
				
				//Get header column indexes
				for(int i = 0; i < headers.length; i++){
					if (headers[i].equalsIgnoreCase(header1))
						x = i;
					else if (headers[i].equalsIgnoreCase(header2))
						y = i;
				}
				if(x == -1 || y == -1){
					System.out.println("Error: a given column identifier does not match with any of the input data headers, exiting...");
					System.exit(-1);
				}
			}
			// Get data
			int lineIndex = -1;
			while ((line = this.bReader.readLine()) != null) {				
				lineData = line.split(",");
				lineIndex++;
				if (lineData.length != headers.length) {					
					System.out.println("Invalid input found at line "
							+ (lineIndex + 2) + ": " + line + " , skipping it...");
					continue;
				}			
				
				//Store data
				try {
					Data newData = new Data(Double.parseDouble(lineData[x]), Double.parseDouble(lineData[y]));
					this.dataList.add(newData);					
				} catch(NumberFormatException nfe){
					System.out.println("Input at line" + lineIndex + " does not contain a numerical value, skipping it...");
					continue;
				}
			}
			if (lineIndex == -1 || this.dataList.size() == 0) {
				System.out.println("Empty input file: exiting...");
				System.exit(2);
			}
		} catch (IOException ioe) {
			System.out.println(ioe.getMessage());
			System.exit(3);
		} 
	}

	public ArrayList<Data> getDataList() {
		return dataList;
	}
}
