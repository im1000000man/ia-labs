package ia.lab17;


import ia.lab17.KMeans;

public class KMeansMain {
	
    public static void main(String[] args)
    {    	
    	Boolean debug = false;
    	if (args.length != 4 && args.length != 5){
    		System.out.println("Usage: kmeans <csv_data_filepath> <num_clusters> <column1_identifier> <column2_identifier> <<DEBUG_FILENAME>>");
    		System.exit(-1);
    	} else if (args.length == 5){
        	debug = true;
    	} 
    	
    	CSVReader csvr = new CSVReader(args[0],args[2], args[3]);
    	int numClusters = 0;
    	try {
    		numClusters = Integer.parseInt(args[1]);
    	} catch (NumberFormatException nfe){
    		System.out.println("Only integer values are allowed as the num_clusters parameter.");
    		System.exit(-1);
    	}
    	
        KMeans kClustering = new KMeans(csvr.getDataList(), numClusters, debug);
              
        kClustering.printClusteringResults();
        
        System.out.println("Centroids finalized at:");
        kClustering.printCentroids();
    }
}
